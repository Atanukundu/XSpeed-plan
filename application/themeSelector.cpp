/*
 * themeSelector.cpp
 *
 *  Created on: 12-Aug-2020
 *      Author: rajarshi
 */

#include <application/themeSelector.h>


themeSelector::themeSelector() {
	// TODO Auto-generated constructor stub
}
void themeSelector::setUserOps(userOptions& op) {
	this->userOps = op;
}

void themeSelector::setInit(std::list<initial_state::ptr>& init){
	this->init = init;
}

void themeSelector::setReachParams(ReachabilityParameters& params) {
	this->reach_params= params;
}

void themeSelector::setForbidden(forbidden_states& forbidden) {
	this->forbidden = forbidden;
}

userOptions& themeSelector::getUserOps(){
	return this->userOps;
}
hybrid_automata::ptr themeSelector::getHaInstance() {
	return ha_ptr;
}
std::list<initial_state::ptr>& themeSelector::getInit() {
	return this->init;
}
ReachabilityParameters& themeSelector::getReachParams(){
	return this->reach_params;
}
forbidden_states& themeSelector::getForbidden(){
	return this->forbidden;
}

void themeSelector::selectReach()
{
	std::list<symbolic_states::ptr> symbolic_states;
	std::list<abstractCE::ptr> ce_candidates; //object of class counter_example

	boost::timer::cpu_timer timer, plottime;
	unsigned int number_of_times = 1;	//For reporting average time
	unsigned int lp_solver_type = 1;	// choose the glpk solver
	timer.start();
	init_cpu_usage(); //initializing the CPU Usage utility to start recording usages
	for (unsigned int i = 1; i <= number_of_times; i++) { //Running in a loop of number_of_times to compute the average result
		// Calls the reachability computation routine.
		reachabilityCaller(*ha_ptr, init, reach_params, userOps, lp_solver_type, forbidden, symbolic_states, ce_candidates);
	}
	timer.stop();
	double cpu_usage = getCurrent_ProcessCPU_usage();
	long mem_usage = getCurrentProcess_PhysicalMemoryUsed();
	print_statistics(timer,cpu_usage,mem_usage, number_of_times, "Reachability Analysis and CE Search");


	// Choosing from the output format and showing results

	if(ha_ptr->ymap_size()!=0){
		//transform the sfm to output directions before plotting
		std::list<symbolic_states::ptr>::iterator it =  symbolic_states.begin();
		for(;it!=symbolic_states.end(); it++){
			symbolic_states::ptr symbStatePtr = *it;
			transformTemplatePoly(*ha_ptr, symbStatePtr->getContinuousSetptr());
		}
	}

	plottime.start();
	//show(symbolic_states, userOps);
	plottime.stop();
//	print_statistics(plottime,"Plotting");

	// printing the first initial polytope in the init_poly file
	polytope::const_ptr init_poly = (*init.begin())->getInitialSet();
	init_poly->print2file("./init_poly",userOps.get_first_plot_dimension(),userOps.get_second_plot_dimension());


}
void themeSelector::selectSim(){
	std::cout << "Running simulation engine ... \n";
	if (forbidden.size() > 0)
		simulationCaller(*ha_ptr, init, reach_params, forbidden[0], userOps);
	else{
		// create an empty forbidden region
		std::pair<int, polytope::ptr> forbidden_s;
		forbidden_s.first = -10; // implies no location
		forbidden_s.second = polytope::ptr(new polytope(true)); // empty polytope
		simulationCaller(*ha_ptr, init, reach_params, forbidden_s, userOps);
	}
}

void themeSelector::selectFal(){
	//todo: call the path-oriented falsification routine.
	boost::timer::cpu_timer timer;
	unsigned int number_of_times = 1;

	bmc bmc_fal(ha_ptr, init, forbidden, reach_params, userOps);

	timer.start();
	init_cpu_usage();

	unsigned int safe = bmc_fal.safe();
	timer.stop();
	double cpu_usage = getCurrent_ProcessCPU_usage();
	long mem_usage = getCurrentProcess_PhysicalMemoryUsed();
	print_statistics(timer,cpu_usage,mem_usage, number_of_times, "Bounded Model Checking");
	
	// printing the first initial polytope in the init_poly file
	polytope::const_ptr init_poly = (*init.begin())->getInitialSet();
	init_poly->print2file("./init_poly",userOps.get_first_plot_dimension(),userOps.get_second_plot_dimension());


	if(safe == 1)
		std::cout << "BMC: The model is SAFE" << std::endl;
	else if(safe == 0)
		std::cout << "BMC: The model is UNSAFE" << std::endl;
	else
		std::cout<<"BMC: The safety of the model is UNKNOWN"<<std::endl;
}

void themeSelector::selectValidation()
{
	std::cout << "Running validation engine ... \n";
	//std::cout<<userOps.get_initial_set()<<endl;
	std::string init_str= userOps.get_initial_set();
	typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
	boost::char_separator<char> sep("&");
	tokenizer tokens(init_str, sep);
	std::list<std::string> all_args;
	for (tokenizer::iterator tok_iter = tokens.begin();
		tok_iter != tokens.end(); ++tok_iter) {
		all_args.push_back((std::string) *tok_iter);
	}
	std::vector<double> point(ha_ptr->getDimension(),0);

	for(std::list<std::string>::iterator iter = all_args.begin(); iter!=all_args.end();iter++)
	{
		std::string tokString = *iter;
		if(tokString.find("=")!=std::string::npos)
		{
			sep = boost::char_separator<char>("==");
			tokens = tokenizer(tokString,sep);
			tokenizer::iterator tok_iter = tokens.begin();
			//std::cout<<(* tok_iter)<<endl;
			std::list<std::pair<double, std::string> > coeff_var_pairs = linexp_parser(*tok_iter);
			unsigned int index;
			for(std::list<std::pair<double, std::string> >::iterator it = coeff_var_pairs.begin(); it!=coeff_var_pairs.end();++it)
			{
				double coeff = (*it).first;
				std::string varname = (*it).second;
				//std::cout<<varname <<"\t"<<coeff<<endl;
				index = ha_ptr->get_index(varname);
			}
			tok_iter++;
			//std::cout<<(*tok_iter)<<endl;
			point[index] = std::atof((*tok_iter).c_str());
		}
	}

/*	std::string init_str= userOps.get_initial_set();
	//init_str.erase(std::remove_if(init_str.begin(), init_str.end(), ::isspace), init_str.end());


	typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
	boost::char_separator<char> sep(" ");
	tokenizer tokens(init_str, sep);

	std::vector<double> point;
	unsigned int max_jump = userOps.get_bfs_level(); //level 0 means only first breadth, 1 means the next level bfs


	for (tokenizer::iterator tok_iter = tokens.begin(); tok_iter != tokens.end(); ++tok_iter)
	{
		point.push_back(std::atof((*tok_iter).c_str()));
	}

	//for (auto it = point.begin(); it != point.end(); ++it)
	  //      cout << ' ' << setprecision(20) <<*it; */

	simulation::ptr sim = simulation::ptr(new simulation());
	unsigned int num_unsafe_traj = 0;
	sim->set_out_dimension(userOps.get_first_plot_dimension(), userOps.get_second_plot_dimension()); //output dimensions
	sim->set_time_step(userOps.get_timeStep());
	sim->set_system_dimension(ha_ptr->getDimension());

	std::vector<sim_start_point> start_pt;
	sim_start_point s;
	s.start_point = point;
	s.cross_over_time = 0;
	s.locptr = ha_ptr->getInitialLocation();
	start_pt.push_back(s);
	boost::timer::cpu_timer sim_time;
	sim_time.start();
	//forbidden[0].second->printPoly();
	bool is_safe = sim->simulateHa(start_pt[0], 0, userOps.get_timeHorizon(), *ha_ptr, forbidden[0], userOps.get_bfs_level());
	if(!is_safe)
	{
		/*//Debug.....
		std::vector<double> st_point = s.start_point;
		std::cout<<"Starting new simulations:\n";
		for (unsigned int i = 0 ; i < st_point.size(); i++)
			std::cout<<st_point[i]<<"\t";
		std::cout<<endl;
		//........*/
		num_unsafe_traj++;
		std::ofstream myfile;
		myfile.open(userOps.getOutFilename().c_str(),std::ofstream::out);
		myfile.close();
		sim->print_trace_to_outfile(userOps.getOutFilename());
		string cmdStr1;
		cmdStr1.append("graph -TX -BC -m 0 ");
		cmdStr1.append(userOps.getOutFilename().c_str());
		system(cmdStr1.c_str());
	}
	sim_time.stop();
	std::cout<<"Is violated trajectory found: "<<num_unsafe_traj<<endl;

	double wall_clock = sim_time.elapsed().wall / 1000000; //convert nanoseconds to milliseconds
	double sim_Time = wall_clock / (double) 1000; //convert to seconds.
	std::cout << "\nSIMULATION TIME::Boost Time taken:Wall  (in seconds) = " << sim_Time << std::endl;
	/*std::ofstream myfile;
	myfile.open(userOps.getOutFilename().c_str(),std::ofstream::out);
	myfile.close();
	sim->print_trace_to_outfile(userOps.getOutFilename());
	//stop showing the plot
	string cmdStr1;
	cmdStr1.append("graph -TX -BC -m 0 ");
	cmdStr1.append(userOps.getOutFilename().c_str());
	system(cmdStr1.c_str()); */
}

void themeSelector::select(){

	// ----Selects trajectory simulation
	if (boost::algorithm::iequals(userOps.getEngine(),"simu")==true) {
		selectSim();
		return;
	}

	// Select reachability with CE generation
	if(boost::algorithm::iequals(userOps.getEngine(),"reach")==true){
		selectReach();
		return;
	}

	//select falsification
	if(boost::algorithm::iequals(userOps.getEngine(),"fal")==true){
		selectFal();
		return;
	}

	//select validation routine for falsification using NN
		if(boost::algorithm::iequals(userOps.getEngine(),"validation")==true){
			selectValidation();
			return;
		}


	// ----Section for Running Exp-Graph. This code is put only for experimental task.
	int	 runExpGraph_WoFC = 0;	// To run Exp-Graph Algorithm, that is, Explore the Graph,
	//we should assign a valid loc-id in the forbidden set (and not -1, unlike FC algo)
	if (runExpGraph_WoFC) {
		bool found_CE = runWoFC_counter_example(*ha_ptr, init, forbidden[0], userOps);

		if (found_CE) {
			string cmdStr1;
			//cmdStr1.append("graph -TX -BC -W 0.008 out.txt -s -m 3 bad_poly -s -m 2 init_poly");
			//cmdStr1.append("graph -TX -BC -W 0.008 out.txt");
			//system(cmdStr1.c_str());
		}
		return;
	}
	//End of Section Exp-Graph.

}
themeSelector::~themeSelector() {

	// TODO Auto-generated destructor stub
}

