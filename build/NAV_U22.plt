set terminal pdf size 6cm, 5.5cm
set style data lines
set zeroaxis

set output "NAV_U22.pdf"

set xlabel "x1" offset 0,0.5
set ylabel "x2" offset 2


set xtics -1,5
#set ytics 0,2
#set xtics add ("" 0)
#set mxtics 0
#show mxtics
#show mytics
#set grid mxtics ,mytics
set grid mxtics ,mytics

set xrange [1 : 23]
set yrange [16 : 27]

plot "out.txt" notitle lt rgb "#dddddd" lw 2,\
     "bad_trace.o" title "trajectory" lt rgb "#5e81b5" lw 4, \
     "bad_poly" title "bad" lt rgb "#eb6235" lw 5, \
     "init_poly"  title "init" lt rgb "#e19c24" lw 5


